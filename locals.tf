locals {
    name   = "myroniuk-lab-5"
    region = "eu-west-2"

    instance_type = "t2.micro"

    volume_type = "gp3"
    volume_encrypted = true
    volume_size = 12


    tags = {
        Name      = "${local.name}-v2"
        ManagedBy = "terraform"
    }
}