provider "aws" {
  region = local.region
}

################################################################################
# EC2 Module
################################################################################

module "lab_5" {
  source       = "terraform-aws-modules/ec2-instance/aws"
  name         = local.name
  putin_khuylo = true

  ami                    = data.aws_ami.ubuntu.id
  ignore_ami_changes     = true
  instance_type          = local.instance_type
  key_name               = module.key_pair.key_pair_name
  vpc_security_group_ids = [module.security_group.security_group_id]
  subnet_id              = data.aws_subnet.selected.id
  availability_zone      = data.aws_subnet.selected.availability_zone

  root_block_device = [
    {
        volume      = local.volume_type
        encrypted   = local.volume_encrypted
        volume_size = local.volume_size
    }
  ]

  tags = local.tags
}

################################################################################
# Supporting Resources
################################################################################

module "key_pair" {
  source = "terraform-aws-modules/key-pair/aws"

  key_name           = "${local.name}-key"
  create_private_key = true
}

module "security_group" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "${local.name}-security-group"
  description = "Security group created in lab5"
  vpc_id      = data.aws_vpc.default.id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  egress_rules        = ["all-all"]
  ingress_rules       = ["http-80-tcp", "https-443-tcp", "ssh-tcp", "postgresql-tcp"]
}